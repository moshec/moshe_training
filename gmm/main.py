
from multiprocessing import Manager, Pool
import numpy as np
from sklearn.mixture import GaussianMixture
import imageio
import time


VIDEO_FILE_PATH = r'Blink Camera System_cut.mp4'
INITIAL_FRAMES = 5
LEARNING_RATE = 1e-2
GMM_COMPONENTS = 5
INITIAL_DECISION_BOUNDARY = 2.5
MAX_PROCESSES = 4


def get_video_reader(file_name):
    return imageio.get_reader(file_name)


def get_index_of_lowest_greater_than_k(value_array, k):
    lowest_valid_index = 0
    for i in range(1, len(value_array)):
        if k < value_array[i] < value_array[lowest_valid_index]:
            lowest_valid_index = i
    return lowest_valid_index


def get_gmm_process_func(shared_namespace, train_set, i, j):
    gmm_model = get_gmm(train_set)
    shared_namespace.models.append([i, j, gmm_model])


def get_gmm(train_set):
    return GaussianMixture(n_components=GMM_COMPONENTS, covariance_type='spherical').fit(train_set)


def main(video_file_path=VIDEO_FILE_PATH):

    # initiate video reader
    # create file reader
    print('preparing training set..')
    video_reader = get_video_reader(video_file_path)

    # get number of frames
    number_of_frames = video_reader.get_length()

    # 1. read k initial frames to decide which distribution is background (B)
    # build train set
    first_frame = np.array(video_reader.get_data(0), dtype=int)
    frame_shape = first_frame.shape
    initial_frame_set = np.zeros((frame_shape[0], frame_shape[1], INITIAL_FRAMES, frame_shape[2]), dtype=int)
    initial_frame_set[:, :, 0] = first_frame
    for i in range(1, INITIAL_FRAMES):
        initial_frame_set[:, :, i] = np.array(video_reader.get_data(i), dtype=int)

    # convert to train set shape- list of pixels
    initial_train_set = initial_frame_set.reshape(frame_shape[0] * frame_shape[1] * INITIAL_FRAMES, frame_shape[2])

    # learn GMM of initial train set
    # print('fitting..')
    # start_time = time.time()
    # initial_gmm = get_gmm(initial_train_set)
    # print('initial fitting time- {fitting_time} secs'.format(fitting_time=time.time()-start_time))
    # print('fitted distributions:\n -> means: {means} \n -> weights: {weights} \n -> covariances: {covariances}'
    #       .format(means=initial_gmm.means_, weights=initial_gmm.weights_, covariances=initial_gmm.covariances_))
    #
    # print('COV*W= {ctw}'.format(ctw=initial_gmm.covariances_ * initial_gmm.weights_))
    #
    # # classify as B the 'strongest' distribution- the one that has the highest weight to variance rate
    # b_dist_index = \
    #     get_index_of_lowest_greater_than_k(initial_gmm.covariances_ * initial_gmm.weights_, k=INITIAL_DECISION_BOUNDARY)
    # b_mean = initial_gmm.means_[b_dist_index]
    # b_weight = initial_gmm.weights_[b_dist_index]
    # print('B distribution- index={b_index} mean={b_mean} weight={b_weight}'
    #       .format(b_index=b_dist_index, b_mean=b_mean, b_weight=b_weight))

    # 2. initiate pixels history
    pixel_history = np.zeros((frame_shape[0], frame_shape[1], number_of_frames, frame_shape[2]), dtype=int)
    pixel_history[:, :, :INITIAL_FRAMES] = initial_frame_set

    # 3. calculate GMM model of each pixel
    manager = Manager()
    shared = manager.Namespace()
    shared.models = manager.list()
    params_list = [
            [shared, pixel_history[i, j, :INITIAL_FRAMES], i, j]
            for j in range(frame_shape[1])
            for i in range(frame_shape[0])
        ]
    pool = Pool(processes=MAX_PROCESSES)
    start_time = time.time()
    pool.starmap(get_gmm_process_func, params_list)
    pool.close()
    pool.join()
    # pixel_models = [
    #     [get_gmm(pixel_history[i, j, :INITIAL_FRAMES]) for j in range(frame_shape[1]) for i in range(frame_shape[0])]
    # ]
    print('models ready. time= {model_time} secs'.format(model_time=time.time()-start_time))
    start_time = time.time()
    local_models_list = list(shared.models)
    pixel_models = [[None] * frame_shape[1]] * frame_shape[0]
    for i in range(len(local_models_list)):
        pixel_i = local_models_list[i][0]
        pixel_j = local_models_list[i][1]
        model = local_models_list[i][2]
        pixel_models[pixel_i][pixel_j] = model

    # 4. for each new frame-
        # 4.1 define as BG pixels that has a distribution which is close to B
        # 4.2 update the GMM model of each pixel

    print('models in grid. time= {model_time} secs'.format(model_time=time.time() - start_time))
    pass


if __name__ == '__main__':
    main()
