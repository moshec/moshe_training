
import pylab
import numpy as np
import imageio
import time

FILE_NAME = 'large.mp4'


def get_video_reader(file_name):
    return imageio.get_reader(file_name,  'ffmpeg')


def get_frame_with_alpha_filter(frame, green_mask, alpha=0.5):
    frame_size = frame.shape[0]
    mask_size = green_mask.shape[0]
    frame_mask = np.full((mask_size, mask_size, 3), 1-alpha)

    # mask frame
    mask_offset = int(frame_size - mask_size) / 2
    float_frame = np.array(frame, dtype=float)
    float_frame[int(mask_offset):int(mask_offset+mask_size), int(mask_offset):int(mask_offset+mask_size), :] \
        *= frame_mask

    float_frame[int(mask_offset):int(mask_offset+mask_size), int(mask_offset):int(mask_offset+mask_size), :] \
        += green_mask

    return np.array(float_frame, dtype=int)


def main():
    # create file reader
    video_reader = get_video_reader(FILE_NAME)

    # get number of frames
    number_of_frames = video_reader.get_length()

    frame = video_reader.get_data(0)
    # transform frame to desired form
    frame = np.array(frame)
    pad_size = max(frame.shape)
    mask_size = int(pad_size / 2)
    green_mask = np.zeros((mask_size, mask_size, 3), dtype=int)
    alpha = 0.3
    green_mask[:, :, 1] = 255 * alpha
    new_frames = list()
    print(number_of_frames)
    print(time.time())
    for frame_index in range(number_of_frames):
        frame = np.array(video_reader.get_data(frame_index))

        # 1. pad to rectangle (add black border)
        padded_frame = np.zeros((pad_size, pad_size, 3), dtype=int)
        row_offset = int((pad_size - frame.shape[0]) / 2)
        column_offset = int((pad_size - frame.shape[1]) / 2)
        padded_frame[row_offset:row_offset+frame.shape[0], column_offset:column_offset+frame.shape[1]] = frame

        # 2. ONLY each **third** frame -- alpha-filter / green mask in the middle of the frame
        if (frame_index % 3) == 0:
            padded_frame = get_frame_with_alpha_filter(padded_frame, green_mask, alpha=alpha)

        new_frames.append(padded_frame)

    # save new video file
    print(time.time())
    pylab.imshow(new_frames[0])
    pylab.show()

    # Audio- calculate mean of each 30ms + plot over time + plot spectrogram
    pass


if __name__ == '__main__':
    main()


#
# for num in nums:
#
#     fig = pylab.figure()
#     fig.suptitle('image #{}'.format(num), fontsize=20)
#     pylab.imshow(image)
# pylab.show()
