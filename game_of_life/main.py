
import time
import numpy as np


BOARD_SIZE = 100


def get_end_value(k):
    return k if k < 0 else BOARD_SIZE


def get_live_neighbours_matrix(game_board):
    total_live_neighbours = np.zeros((BOARD_SIZE, BOARD_SIZE))
    shifts = np.array([-1, 0, 1])
    for i in shifts:
        for j in shifts:
            if i == 0 and j == 0:
                continue

            op_i = i * -1
            op_j = j * -1

            total_live_neighbours[
                max(0, i): get_end_value(i)
                , max(0, j): get_end_value(j)
            ] += \
                game_board[
                    max(0, op_i): get_end_value(op_i)
                    , max(0, op_j): get_end_value(op_j)
                ]
    return total_live_neighbours


def main():
    # create initial generation (randomized)
    game_board = np.random.random((BOARD_SIZE, BOARD_SIZE))
    game_board[game_board > 0.5] = 1
    game_board[game_board <= 0.5] = 0
    print(game_board)

    # forever- apply game rules and produce next generations
    start_time = time.time()
    stopped = False
    episodes_to_play = int(1e5)
    passed = 0
    for i in range(episodes_to_play):
        live_neighbours_matrix = get_live_neighbours_matrix(game_board)
        joined_matrix = live_neighbours_matrix + game_board
        next_live_cells_indices = ((joined_matrix > 2) * (joined_matrix < 5))
        game_board[:, :] = 0
        game_board[next_live_cells_indices] = 1
        # print('----------------')
        # print(game_board)
        if np.count_nonzero(game_board) == 0:
            print('-- game ended after {i} episodes'.format(i=i))
            passed = i+1
            stopped = True
            break

    if not stopped:
        print('last generation: ')
        print(game_board)
        passed = episodes_to_play

    total_seconds = time.time() - start_time
    print('total time: ', total_seconds, ' secs')
    print('avg. seconds per episode: ', total_seconds/passed)


if __name__ == '__main__':
    main()
