
import os
import time
import threading


class Logger(object):

    LOG_FOLDER_NAME = 'logs'
    LOG_FILE_NAME = 'log__{timestamp}.log'

    def __init__(self):
        # create logging lock
        self.__log_lock = threading.Lock()

        # verify logs folder exists
        if not os.path.exists(Logger.LOG_FOLDER_NAME):
            os.mkdir(Logger.LOG_FOLDER_NAME)

        # create log file
        self.__log_file_path = \
            os.path.join(Logger.LOG_FOLDER_NAME, Logger.LOG_FILE_NAME.format(timestamp=time.ctime()))
        self.log('Logger created')

    def log(self, msg, should_print=True, should_store=True):

        # acquire lock
        with self.__log_lock:
            # format message
            formatted_message = '[{timestamp}] >> {msg}'.format(timestamp=time.ctime(), msg=msg)

            # print message if required
            if should_print:
                print(formatted_message)

            # store message if required
            if should_store:
                with open(self.__log_file_path, 'at') as log_file:
                    log_file.write(formatted_message + '\n')

