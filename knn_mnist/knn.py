
import numpy as np
from scipy.stats import mode


class KNN:
    def __init__(self, data, labels, k):
        # this is the 'training'..
        self.__data = data
        self.__labels = labels
        self.__k = k

    def predict(self, sample):
        differences = (self.__data - sample)
        distances = np.einsum('ij, ij->i', differences, differences)
        nearest = self.__labels[np.argsort(distances)[:self.__k]]
        return mode(nearest)[0][0]

    @staticmethod
    def reduce_dimensions_using_svd(data, k):
        formatted_data = data.astype('float64')
        formatted_data -= np.mean(formatted_data, axis=0)
        u, s, _ = np.linalg.svd(formatted_data, full_matrices=False)
        return u[:, :k].dot(np.diag(s[:k]))
