
import random
import numpy as np
import pickle
import time
from knn import KNN

import sys
sys.path.insert(0, '../utils')
from logger import Logger


validation_rate = 0.25
TRAIN_DATASET_FILE_PATH = r'dataset/train.csv'
TEST_DATASET_FILE_PATH = r'dataset/test.csv'
TRAIN_PKL_FILE_PATH = r'dataset/train.pkl'
TEST_PKL_FILE_PATH = r'dataset/test.pkl'


def prepare_dataset(dataset_file_path, pickle_file_path):
    print('working on: ', dataset_file_path)
    dataset = np.genfromtxt(dataset_file_path, delimiter=',', skip_header=1).astype(np.dtype('uint8'))
    with open(pickle_file_path, 'wb') as pickle_file:
        pickle.dump(dataset, pickle_file)
    print('created: ', pickle_file_path)


def read_dataset_file(dataset_file_path):
    print('reading: ', dataset_file_path)
    with open(dataset_file_path, 'rb') as pickle_file:
        return pickle.load(pickle_file)


def main():
    # create logger
    logger = Logger()

    # read train data
    train_file_data = read_dataset_file(TRAIN_PKL_FILE_PATH).astype(np.int32)

    # extract samples and labels
    all_samples = train_file_data[:, 1:]
    all_labels = train_file_data[:, 0]

    # reduce dimensions
    dimensions = 45
    logger.log('reduce dimensions: k={k}'.format(k=dimensions))
    all_samples = KNN.reduce_dimensions_using_svd(all_samples, k=dimensions)

    # split to test and validation
    all_indices = list(range(all_samples.shape[0]))
    random.shuffle(all_indices)
    validation_size = int(all_samples.shape[0] * validation_rate)
    train_indices = all_indices[:-validation_size]
    validation_indices = all_indices[-validation_size:]

    train_x = all_samples[train_indices]
    train_y = all_labels[train_indices]
    validation_x = all_samples[validation_indices]
    validation_y = all_labels[validation_indices]

    # build KNN classifier
    k_nearest = 6
    knn_classifier = KNN(train_x, train_y, k=k_nearest)

    # calculate error on validation set
    error_count = 0
    start_time = time.time()
    logger.log('validation samples: {validation_length}'.format(validation_length=validation_x.shape[0]))
    for sample_index in range(validation_x.shape[0]):
        predicted = knn_classifier.predict(validation_x[sample_index])
        if predicted != validation_y[sample_index]:
            error_count += 1
        if (sample_index % 100) == 0 and sample_index > 0:
            logger.log('samples: {sample_index} | loss: {loss:.4f}'
                       .format(sample_index=sample_index, loss=error_count / (sample_index + 1)))

    # print error rate
    logger.log('total time: {run_time:.3f} secs'.format(run_time=time.time()-start_time))
    logger.log('correct rate: {correct_rate:.4f}'.format(correct_rate=1-(error_count/validation_x.shape[0])))


if __name__ == '__main__':

    # prepare_dataset(TRAIN_DATASET_FILE_PATH, TRAIN_PKL_FILE_PATH)
    # prepare_dataset(TEST_DATASET_FILE_PATH, TEST_PKL_FILE_PATH)

    main()
