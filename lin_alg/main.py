
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import numpy as np
import cv2
import sys


def flat_image(original_img_arr, colors=3):
    img_rows = original_img_arr.shape[0]
    img_cols = original_img_arr.shape[1]

    return original_img_arr.reshape(img_rows, img_cols * colors)

    # flatted_arr = np.zeros((3, img_rows * img_cols))
    # for i in range(3):
    #     flatted_arr[i, :] = original_img_arr[:, :, i].reshape(img_rows * img_cols)
    #
    # return flatted_arr


def reshape_image(flatted_img_arr, img_rows, img_cols, colors=3):

    return flatted_img_arr.reshape(img_rows, img_cols, colors)

    # reshaped_image = np.zeros((img_rows, img_cols, 3))
    # for i in range(3):
    #     reshaped_image[:, :, i] = flatted_img_arr[i, :].reshape(img_rows, img_cols)
    # return reshaped_image


def get_reconstructed_image(u, s, v, k):
    # k is number of highest values to use
    s_diagonal = np.diag(s[:k])
    v_reduced = v[:k, :]
    s_v = s_diagonal.dot(v_reduced)
    u_reduced = u[:, :k]
    reconstructed_image_flatted = u_reduced.dot(s_v)
    return reconstructed_image_flatted


def show_images_together(image_list, window_name):
    image_count = len(image_list)
    figure = plt.figure(figsize=(16, 8), num=window_name)
    for i in range(image_count):
        sub_plot = figure.add_subplot(1, image_count, i+1)
        sub_plot.set_title(image_list[i][1])
        sub_plot.imshow(image_list[i][0])

    plt.show()


def main(image_file_path):
    # read image file
    image_array = cv2.imread(image_file_path)

    # create random distortion
    distortion_mask = np.random.normal(size=image_array.shape) * 25
    # distortion_mask = 2 * np.random.random(size=image_array.shape) - 1.0

    # apply distortion to loaded image
    distorted_image = np.array(image_array + distortion_mask, dtype=int)

    # # show images
    # show_images_together([
    #     [image_array, 'Original image'],
    #     [distorted_image, 'Distorted image']
    # ])

    # flat image
    flatted_original_image = flat_image(image_array)
    flatted_distorted_image = flat_image(distorted_image)
    # print initial MSE score
    print('Initial RMSE= {rmse_value}'.format(
            rmse_value=np.sqrt(mean_squared_error(flatted_original_image, flatted_distorted_image))
        )
    )

    # 1. reduce noise using svd
    # [k=19] Reconstructed RMSE= 11.908282717542496
    # [k=19] Reconstructed RMSE= 11.907240220532398
    # [k=19] Reconstructed RMSE= 11.851112261393308
    u, s_values, v = np.linalg.svd(flatted_distorted_image, full_matrices=True)
    best_k = 19
    print('Max best_k= {s_size}'.format(s_size=s_values.shape[0]))
    reconstructed_flatted_image = get_reconstructed_image(u, s_values, v, best_k)
    print('[k={k}] Reconstructed RMSE= {mse_value}'.format(
            k=best_k, mse_value=np.sqrt(mean_squared_error(flatted_original_image, reconstructed_flatted_image))
        )
    )

    # show images
    reshaped_reconstructed_image = \
        np.array(reshape_image(reconstructed_flatted_image, image_array.shape[0], image_array.shape[1]), dtype=int)
    show_images_together([
        [image_array, 'Original image'],
        [distorted_image, 'Distorted image'],
        [reshaped_reconstructed_image, 'Reconstructed image']
    ], window_name='SVD reconstruction')

    # show RMSE(K) graph
    k_values = np.arange(1, s_values.shape[0])
    rmse_values = np.zeros(k_values.shape[0])
    for k_index in range(rmse_values.shape[0]):
        u, s_values, v = np.linalg.svd(flatted_distorted_image, full_matrices=True)
        reconstructed_flatted_image = get_reconstructed_image(u, s_values, v, k_values[k_index])
        rmse_values[k_index] = np.sqrt(mean_squared_error(flatted_original_image, reconstructed_flatted_image))
    plt.plot(k_values, rmse_values)
    plt.show()


if __name__ == '__main__':
    # image_path = sys.argv[1]
    image_path = r'cat.jpeg'
    main(image_path)
