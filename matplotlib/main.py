
import numpy as np
import matplotlib.pyplot as plt


def main():

    # ## create series
    # create indices
    t_min = 1.0
    t_max = 10.0
    sample_spacing = 0.01
    sample_frequency = 1 / sample_spacing
    t_values = np.arange(t_min, t_max, sample_spacing)
    number_of_samples = t_values.shape[-1]

    # create series y values
    y_values = np.sin(t_values) + np.sin(t_values * 10000)

    # ## calculate FFT for series
    # calculate raw FFT - result is in complex numbers form
    fft_y_complex = np.fft.fft(y_values)
    # convert values to their magnitude
    fft_y_magnitude = np.absolute(fft_y_complex)
    # normalize the values - divide by sample count
    fft_y_normalized = fft_y_magnitude / number_of_samples

    # ## calculate frequencies
    fft_frequencies = np.fft.fftfreq(n=number_of_samples, d=sample_spacing)
    # todo: support one side FFT

    # ## plot result
    highest_magnitude_indices = np.argwhere(fft_y_normalized > 0.3)
    print(
        'active frequencies: ',
        ', '.join(str(x[0]) for x in fft_frequencies[highest_magnitude_indices] if x[0] > 0)
    )
    plt.plot(fft_frequencies, fft_y_normalized, '.')
    plt.show()


if __name__ == '__main__':
    main()
