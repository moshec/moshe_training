
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
import cv2
import time


IMAGE_FILE_PATH = r'parrots.jpg'
NUMBER_OF_COLORS = 100


def image_sample_set_to_2d(image3d, original_shape):
    return np.array(image3d.reshape(original_shape), dtype=int)


def image_2d_to_sample_set(image_2d):
    rows = image_2d.shape[0]
    cols = image_2d.shape[1]
    return np.array(image_2d.reshape(rows * cols, 3), dtype=int)


def main():
    # read image file
    image_rgb_array = cv2.imread(IMAGE_FILE_PATH)
    original_shape = image_rgb_array.shape
    print('image shape= {im_shape}'.format(im_shape=original_shape))

    # train classifier
    print('training..')
    start_time = time.time()
    image_sample_set = image_2d_to_sample_set(image_rgb_array)
    kmeans_classifier = KMeans(n_clusters=NUMBER_OF_COLORS, random_state=0, n_jobs=8)

    # quantize image pixels
    kmeans_classifier = kmeans_classifier.fit(image_sample_set)
    train_time = time.time() - start_time
    quantize_image = kmeans_classifier.cluster_centers_[kmeans_classifier.predict(image_sample_set)]

    # plot quantize image
    print('train time= {train_time} secs'.format(train_time=train_time))
    print('pred. time= {pred_time} secs'.format(pred_time=time.time()-start_time-train_time))
    plt.imshow(image_sample_set_to_2d(quantize_image, original_shape))
    plt.show()


if __name__ == '__main__':
    main()
