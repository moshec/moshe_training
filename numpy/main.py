
import numpy as np


def main():

    # # Consider a set of p matrices which shape (n,n) and a set of p vectors with shape (n,1).
    # # How to compute the sum of the p matrix products at once? (result has shape (n,1))
    # mx_1 = np.array([
    #     [1, 2, 3],
    #     [3, 4, 5],
    #     [5, 6, 7]
    # ])
    # v_1 = np.array([2, 4, 6])
    #
    # mx_set = np.array([mx_1, mx_1+1])
    # v_set = np.array([v_1, v_1+1])
    # product_sum = sum(mx_set[i].dot(v_set[i]) for i in range(len(mx_set)))

    # # How to find the most frequent value in an array?
    # arr_values = np.array([1, 2, 3, 4, 5, 6, 4, 1, 3, 4, 2, 2, 3, 5, 6, 7, 4, 3, 2, 4, 4, 6, 7, 4, 3, 2, 2, 2, 3,
    #                        4, 5, 6, 3, 4, 5, 6, 4, 3, 4, 5, 6, 4, 3, 2, 5, 6, 6, 3, 2, 5, 5], dtype=np.float)
    # for i in np.arange(len(arr_values)):
    #     rand_value = np.random.rand()
    #     if rand_value < 0.5:
    #         arr_values[i] *= -1
    #
    # values, counts = np.unique(arr_values, return_counts=True)
    # max_count_index = np.argmax(counts)
    # most_frequent_value = values[max_count_index]

    # # Compute a matrix rank
    # arr_rank = np.array(
    #     [
    #         [2, 4, 4, 4],
    #         [0, 2, 1, 2],
    #         [0, 0, 1, 1],
    #         [0, 0, 0, 0]
    #     ],
    #     dtype=np.float
    # )
    # matrix_rank = np.linalg.matrix_rank(arr_rank)

    # # Consider 2 sets of points P0,P1 describing lines (2d) and a point p, how to compute distance from p to each
    # # line i (P0[i],P1[i])?
    # p0_arr = np.array([[0, 3], [3, 7], [2, 6]])
    # p1_arr = np.array([[1, 2], [2, 3], [3, 4]])
    # p2 = np.array([2, 3])
    #
    # p1_minus_p0 = p1_arr - p0_arr
    # p0_minus_p2 = p0_arr - p2
    # cross_value = np.cross(p1_minus_p0, p0_minus_p2)
    # distances = cross_value / np.linalg.norm(p1_minus_p0)

    # # running average with window size
    # window_size = 4
    # x = np.arange(1, 10)
    # cs = np.cumsum(np.insert(x, 0, 0))
    # moving_averages = (cs[window_size:] - cs[:-window_size]) / np.float(window_size)

    # # Consider the vector [1, 2, 3, 4, 5], how to build a new vector with 3 consecutive zeros interleaved between
    # # each value?
    # arr_a = np.array([1, 2, 3, 4, 5])
    # arr_b = np.zeros((5, 17))
    # arr_b[np.arange(len(arr_a)), [i * 4 for i in range(len(arr_a))]] = 1
    # arr_a.dot(arr_b)

if __name__ == '__main__':
    main()