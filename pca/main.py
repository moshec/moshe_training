
import numpy as np
from sklearn.decomposition import PCA


# define vector set
X = np.array(
    [
        [-1, -1],
        [-2, -1],
        [-3, -2],
        [1,   1],
        [2,   1],
        [3,   2]
    ]
)

# create PCA instance
pca = PCA(n_components=2)

# fit on vector set
pca.fit(X)
# PCA(copy=True, iterated_power='auto', n_components=2, random_state=None, svd_solver='auto', tol=0.0, whiten=False)

print(pca.explained_variance_ratio_)
# [ 0.99244...  0.00755...]

print(pca.singular_values_)
# [ 6.30061...  0.54980...]
